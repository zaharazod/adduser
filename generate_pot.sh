#!/bin/bash
# This file is distributed under the same license as the adduser package.
# This process is currently being tested. It is possible that it might go again
#
# The program here is being used to regenerated POT and PO files for 
# the program translations of adduser.
#
# After Generation:
# Create e-mail messages with
# podebconf-report-po --verbose --call --deadline=2025-03-31 --notdebconf --package=adduser --postpone=/tmp/acall --utf8 --withtranslators --gzip
# call mutt, type :set postponed=/tmp/acall
# Call up postpones messages (R) and  send one by one.
#
# It is suggested to commit the POT file immediately and the PO files only
# when they have been touched by a translator.

# Define file names
HEADER_FILE="po/header.pot"
TEMP_FILE="po/temp.pot"
POT_FILE="po/adduser.pot"
SOURCE_FILES="adduser deluser *.pm"

# Create a custom header file if it doesn't exist
if [ ! -f "${HEADER_FILE}" ]; then
    cat <<EOL > "${HEADER_FILE}"
# adduser program translation
# Copyright (C) (insert year and author here)
# This file is distributed under the same license as the adduser package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: adduser $(dpkg-parsechangelog --show-field Version)\n"
"Report-Msgid-Bugs-To: adduser@packages.debian.org\n"
"POT-Creation-Date: $(date +"%Y-%m-%d %H:%M%z")\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Your Name <your.email@example.com>\n"
"Language-Team: MyProject Team <team@example.com>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
EOL
fi

# Extract strings without generating a default header
xgettext --keyword=mtx --keyword=gtx --omit-header -o "${TEMP_FILE}" --from-code=UTF-8 -L ${SOURCE_FILES}

# Merge custom header and extracted strings
cat "${HEADER_FILE}" "${TEMP_FILE}" > "${POT_FILE}"

# Clean up temporary file
rm "${TEMP_FILE}" "${HEADER_FILE}"

echo "POT file generated: ${POT_FILE}"

# Loop through all .po files in the locale directory
for PO_FILE in po/*.po; do
    if [ -f "${PO_FILE}" ]; then
        echo "Updating ${PO_FILE}..."
        msgmerge --update --backup=none --no-fuzzy-matching "${PO_FILE}" "${POT_FILE}"
    fi
done

echo "PO files generation finished"

